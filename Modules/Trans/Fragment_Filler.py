from Bio import PDB
from Bio.PDB import PDBParser, NeighborSearch, Superimposer, PDBIO
from Bio.PDB.Atom import Atom
from Bio.PDB.Residue import Residue
from Bio.PDB.Chain import Chain
from Bio.PDB.Model import Model
from Bio.PDB.Structure import Structure
from Bio.PDB.PDBIO import Select
from StringIO import StringIO
from Bio.PDB import *
from extres import Extres
import random
import sys, os

class FragmentError(Exception):pass
class InputError(Exception):pass

prot_db_path = "Fragment_databases/baza/"
prot_db_lib_file = "Fragment_databases/proteinfr.lib"
rna_db_path = "Fragment_databases/rnaDB05/"
rna_db_lib_file = 'Fragment_databases/LIR_fragments.lib'

#MOL_TYPE = "RNA"

class Fragment():
    """
    fragment of particular length from the DATABASE
    """
    def __init__(self, seq, sec_struct, length, anchoring_distance, frag_name, frag_struct):
        self.sequence = seq
        self.sec_structure = sec_struct
        self.length = length
        self.dist_between_anchoring_residues = anchoring_distance
        self.fragment_name = frag_name
        self.frag_struct = frag_struct #BioPDBParser

class Gap():
    """
    empty fragment in the model
    """
    def __init__(self):
        self.sequence = ""
        self.sec_structure = ""
        self.length = 0
        self.gaps_positions = []
        self.fr_type = "" #n_term, c_term, internal
        self.distance = 0.0


    def get_sequence(self,seq):
        self.sequence = seq[self.gaps_positions[0]-1:self.gaps_positions[1]]
        print self.gaps_positions[0]-1, self.gaps_positions[1]
        print "SEQ LUKI W KOMPONENCIE", self.sequence

    def get_sec_struct(self, sec_struct):
        self.sec_structure = sec_struct[self.gaps_positions[0]-1:self.gaps_positions[1]]
        print self.gaps_positions[0]-1, self.gaps_positions[1]
        print "SS LUKI W KOMPONENCIE", self.sec_structure

    def get_anchoring_distance(self, at_start, at_end ):
        self.distance = -abs(at_start - at_end)

    def calculate_length(self):
        self.length = self.gaps_positions[1] - self.gaps_positions[0] + 1
        print "LEN", self.length, type(self.length)

    def check_gap_type(self, seq):
        if self.gaps_positions[0]==1:
            self.fr_type = "n_term"
        elif self.gaps_positions[1]==len(seq):
            self.fr_type = "c_term"
        else:
            self.fr_type = "internal"

class Superimpose_gap():
    def __init__(self,mol_type):
        self.rmsd = 0.0
        self.MOL_TYPE = mol_type
        if self.MOL_TYPE == "RNA":
            self.backbone_names = ["C4'", "O5'", "C5'"]
        if self.MOL_TYPE == "protein":
            self.backbone_names = ['N', 'CA', 'C', 'O']
        self.fixed = []
        self.moving = []
        self.sup = None #superimposer class instance

    def superimpose_data_internal(self, chain, gap, fragment):
        model_anchor_start_res = chain[(' ', gap.gaps_positions[1] + 1 , ' ')]
        model_anchor_end_res = chain[(' ', gap.gaps_positions[0] - 1 , ' ')]
        fragment_anchor_start_res = fragment.frag_struct[(' ', fragment.frag_struct.child_list[-1].id[1], ' ')]
        fragment_anchor_end_res = fragment.frag_struct[(' ', fragment.frag_struct.child_list[0].id[1], ' ')]

        for name in self.backbone_names:
            self.fixed.append(model_anchor_start_res[name])

        for name in self.backbone_names:
            self.fixed.append(model_anchor_end_res[name])

        for name in self.backbone_names:
	    print "dzieciniedzialajace 1", fragment_anchor_start_res.child_list
            self.moving.append(fragment_anchor_start_res[name])

        for name in self.backbone_names:
	    print "dzieciniedzialajace 2", fragment_anchor_end_res.child_list
            self.moving.append(fragment_anchor_end_res[name])

    def superimpose_data_Cterm(self, chain, gap, fragment):
        model_anchor_res = chain[(' ', gap.gaps_positions[0] - 1 , ' ')]
        fragment_anchor_res = fragment.frag_struct[(' ', fragment.frag_struct.child_list[0].id[1], ' ')]
        
        for name in self.backbone_names:
            self.fixed.append(model_anchor_res[name])

        for name in self.backbone_names:
	    print "dzieciniedzialajace 3", fragment_anchor_res.child_list
            self.moving.append(fragment_anchor_res[name])

    def superimpose_data_Nterm(self, chain, gap, fragment):

        model_anchor_res = chain[(' ', gap.gaps_positions[1] + 1, ' ')]   
        fragment_anchor_res = fragment.frag_struct[(' ', fragment.frag_struct.child_list[-1].id[1], ' ')]

        #fragment_anchor_res = fragment.frag_struct.child_list[-2]

        for name in self.backbone_names:
            self.fixed.append(model_anchor_res[name])

        for name in self.backbone_names:
	    print "dzieciniedzialajace 4", fragment_anchor_res.child_list
            self.moving.append(fragment_anchor_res[name])

    def calculate_rt_matrix(self):
        self.sup = Superimposer()

        self.sup.set_atoms(self.fixed,self.moving)
        self.rmsd = self.sup.rms

    def apply_rt_matrix(self, frag):
        fragment_atoms_list = []

        for resi in frag.frag_struct:
            for  atom in resi:
                fragment_atoms_list.append(atom)
        self.sup.apply(fragment_atoms_list)

    def rename_fragment(self, component, fragment):
        chain_id = [chain.id for chain in component.get_chains()]
        for mod in fragment.frag_struct:
            for chain in mod:
                chain.id = chain_id[0]

    def renumber_fragment(self, fragment, gap):
        count = 0
        chain = fragment.frag_struct
        
        new_child_dict = {}

        if gap.fr_type == "n_term":
            for i in range(gap.gaps_positions[0], gap.gaps_positions[1]+2):
                resi = chain.child_list[count]
                resi.id = (" ",i, " ")
                new_child_dict[resi.id] = resi
                count += 1
        elif gap.fr_type == "internal":
            for i in range(gap.gaps_positions[0], gap.gaps_positions[1]+3):
                resi = chain.child_list[count]
                resi.id = (" ",i-1, " ")
                new_child_dict[resi.id] = resi
                count += 1
        elif gap.fr_type == "c_term":
            for i in range(gap.gaps_positions[0], gap.gaps_positions[1]+2):
                resi = chain.child_list[count]
                resi.id = (" ",i-1, " ")
                new_child_dict[resi.id] = resi
                count += 1
        chain.child_dict = new_child_dict
        
    def merge_fragment_into_model(self, marged_structure, fragment, gap_type):
        model_residues = list(marged_structure.get_residues())
        fragment_residues = list(fragment.frag_struct.get_residues())
        
        count = 1

        if gap_type == 'n_term':
	    fragment.frag_struct.detach_child(fragment_residues[-1].id)
            fragment_residues = fragment_residues[:-1]
            fragment.sequence = fragment.sequence[:-1]
            fragment.sec_structure = fragment.sec_structure[:-1]
            print type(fragment.length)
            
        elif gap_type == 'c_term':
	    fragment.frag_struct.detach_child(fragment_residues[0].id)
            fragment_residues = fragment_residues[1:]
            fragment.sequence = fragment.sequence[1:]
            fragment.sec_structure = fragment.sec_structure[1:]
            fragment.length-=1
            
        elif gap_type == 'internal':
	    fragment.frag_struct.detach_child(fragment_residues[0].id)
            fragment.frag_struct.detach_child(fragment_residues[-1].id)
            fragment_residues = fragment_residues[1:-1]
            fragment.sequence = fragment.sequence[1:-1]
            fragment.sec_structure = fragment.sec_structure[1:-1]
            fragment.length-=2

        all_residues = model_residues + fragment_residues
            
        all_residues.sort(key=lambda Residue: Residue.id[1], reverse = False)

        for resi in all_residues:
            for atom in resi.child_list:
                atom.id = count
                count += 1

        return all_residues, fragment_residues, fragment


    def build_merged_structure(self, all_residues, component):
        chain_id = [chain.id for chain in component.get_chains()][0]
        merged_structure = Structure.Structure('str')

        my_model = Model.Model(0)
        my_chain = Chain.Chain(chain_id)
        merged_structure.add(my_model)
        my_model.add(my_chain)
        
        fff = open("bug.txt","w")
        
        
        print >> fff, "allresi"
        for r in all_residues:
            print >> fff, r
        print >> fff, "-----------------------"
        print >> fff, "mychain"
        for r in my_chain.child_list:
            print >> fff, r
        
        fff.close()

        for resi in all_residues:
            my_chain.add(resi)

        return merged_structure

    def write_pdb(self, merged_structure, outname):
        outfile=open(outname, "w")
        io = PDBIO(1)
        io.set_structure(merged_structure)
        io.save(outfile)
        outfile.close()


class Scorer():
    def __init__(self):
        self.seq_coverage = 0.0
        self.sec_struct_coverage = 0.0
        self.rmsd_score = 0.0
        self.anchoring_distance = 0.0

    def score_sequence(self, gap, frag_sequence):
        """
        Compares the missing fragment's sequence with
        the sequence of the fragment from the database.
        Returns 0 if the match is perfect, -1 if there's
        one mismatch, etc.
        Check if gap is on stard/end of sequence or in.
        """
        #the method is divided into missing gap at the beginning/end and inside the protein structure
        if gap.fr_type == "n_term" or gap.fr_type == "c_term":
            for i in range(0, gap.length):
	        #print "porownuje", gap.sequence[i], frag_sequence[i]
                if gap.sequence[i] != frag_sequence[i]:
                    
                    self.seq_coverage -= 1
        else:
            for i in range(0, gap.length):
	        #print "porownuje", gap.sequence[i], frag_sequence[i]
                if gap.sequence[i] != frag_sequence[i]:
                    self.seq_coverage -= 1
        return self.seq_coverage
        

    def score_secondary_structure(self, gap, frag_sec_structure):
        """
        Compares the missing fragment's secondary structure with
        the secondary structure of the fragment from the database.
        Returns 0 if the match is perfect, -1 if there's
        one mismatch, etc.
        """
        if gap.fr_type == "n_term" or gap.fr_type == "c_term":
            for i in range(0, gap.length):
		#print "porownuje", gap.sec_structure[i].upper(), frag_sec_structure[i].upper()
                if gap.sec_structure[i].upper() != frag_sec_structure[i].upper():
                    self.sec_struct_coverage -= 1
        elif gap.fr_type == "internal":
            for i in range(0, gap.length):
	        #print "porownuje", gap.sec_structure[i].upper(), frag_sec_structure[i].upper()
                if gap.sec_structure[i].upper() != frag_sec_structure[i].upper():
                    self.sec_struct_coverage -= 1
        return self.sec_struct_coverage

    def score_anchoring_dist(self, dist1, dist2):
        self.anchoring_distance = -abs(dist1 - dist2)
        return self.anchoring_distance

class FASTAParser():
    def __init__(self, fasta_file, mol_type):
        """
        init should call check_fasta_format method and raise Exeption 
        if it is incorrect
        """
        self.MOL_TYPE = mol_type
        self.sequence = []
        self.sec_struct = ''
        self.fh = open(fasta_file,'r').readlines()
        self.check_fasta_format(fasta_file)

    def check_fasta_format(self, fasta_file):
        #checks fasta
        if len(self.fh) != 3:
            raise InputError("FASTA file must contain 3 lines!")
        if len(self.fh[1].strip('\n')) != len(self.fh[2].strip('\n')):
            raise InputError("Sequence and secondary structure must be\
                                                       the same length")
        if self.MOL_TYPE == "RNA":
            if any(c not in ('.', '(', ')') for c in self.fh[2]):
                raise InputError("Secondary structure must only contain\
                                                        '.','(' or ')'")
        if self.MOL_TYPE == "protein":
            if any(c not in ('e', 'h', '-') for c in self.fh[2].lower()):
                raise InputError("Secondary structure must only contain\
                                                        'h','e' or '-'")

    def get_sequence(self):
        """
        put seqence into self.sequence
        """
        for i in self.fh[1].strip('\n'):
            self.sequence.append(i)
        return self.sequence

    def get_sec_struct(self):
        """
        put seqence into self.sequence
        """
        for j in self.fh[2].strip('\n'):
            self.sec_struct +=j
        return self.sec_struct

class ModelParser():
    def __init__(self, mol_type):
        """
        raise error if the format of PDB file is wrong
        """
        self.MOL_TYPE = mol_type
        self.gaps = []
        self.sequence = ''
        self.structure = None #BioPDBParser object
        self.gaps_positions = []
        self.component_chain_id = ''


    def check_pdb_format(self):

        if(self.structure.get_residues() == 0):
            raise InputError("Wrong PDB file format!")

    def get_structure(self, pdb_file):
        """
        """
        parser = PDBParser()
        self.structure = parser.get_structure('str',pdb_file)
        self.check_pdb_format()
        return self.structure

    def get_all_gaps(self, fasta_sequence, fasta_sec_struct):
        """

        """
        length_fasta_seq = len(fasta_sequence)
        allgaps = self.get_start_end_gaps(length_fasta_seq)

        for i in range(0, allgaps):
            g = Gap()
            g.gaps_positions = self.gaps_positions[i]
            g.get_sequence(fasta_sequence)
            g.get_sec_struct(fasta_sec_struct)
            at_start, at_end, self.component_chain_id = self.get_gap_atoms(length_fasta_seq)
            g.get_anchoring_distance(at_start, at_end)
            g.calculate_length()
            g.check_gap_type(fasta_sequence)
            self.gaps.append(g)
        return self.gaps

    def get_start_end_gaps(self, length_fasta_seq):
        res_num = []
        residues = self.structure.get_residues()

        for res in residues:
            res_num.append(res.id[1])

        for i, j in enumerate(res_num):
            if i==0 and res_num[i]!="1":
                start = 1
                end = res_num[i]-1
                self.gaps_positions.append([start, end])
            elif i!=len(res_num)-1:
                if res_num[i]-res_num[i-1]>1:
                    start = res_num[i-1]+1
                    end = res_num[i]-1
                    self.gaps_positions.append([start, end])      
            elif i==len(res_num)-1 and res_num[i]<length_fasta_seq:
                start = res_num[i]+1
                end = length_fasta_seq
                self.gaps_positions.append([start, end])
                            
            if end - start > 30:
                raise FragmentError("The missing fragment is longer than 30!")
        
        print "xxxxxxx POZYCJE LUK", self.gaps_positions
        return len(self.gaps_positions)

    def get_gap_atoms(self, length_fasta_seq):
        chain_id = [chain.id for chain in self.structure.get_chains()]

        if self.MOL_TYPE == "RNA":
            for i in range(0, len(self.gaps_positions), 2):
                if self.gaps_positions[i][0] != 1:
                    at_start = self.structure[0][chain_id[0]][(' ',self.gaps_positions[i][0]-1, ' ')]['C4']
                if self.gaps_positions[i][1]<length_fasta_seq:
                    at_end = self.structure[0][chain_id[0]][(' ', self.gaps_positions[i][1]+1, ' ')]['C4']
        if self.MOL_TYPE == "protein":
            for i in range(0, len(self.gaps_positions)):
                if self.gaps_positions[i][0] != 1:
                    at_start = self.structure[0][chain_id[0]][(' ',self.gaps_positions[i][0]-1, ' ')]['CA']
                if self.gaps_positions[i][1]<length_fasta_seq:
                    at_end = self.structure[0][chain_id[0]][(' ', self.gaps_positions[i][1]+1, ' ')]['CA']

        return at_start, at_end, chain_id

class DatabaseParser():
    def __init__(self, db_file, mol_type):
	self.MOL_TYPE = mol_type
        self.lib_lines = []
        self.db_file = db_file
        self.fh = open(db_file, "r").readlines()
        self.open_db(db_file)
        self.fragments_of_given_length = {}

    def open_db(self, db_file):
        for i in self.fh:
            if len(i)>6:
                self.lib_lines.append(i)

    def get_fragments_of_given_length(self):
        """
        return dictionary where keys are fragment length and values are file names of fragments with given length
        """
        for h,i in enumerate(self.lib_lines):
            if h!=0:
                line = i.split("\t")
                key = int(line[0])
                if self.fragments_of_given_length.has_key(key):
                    self.fragments_of_given_length[key].append(line)
                else:
                    self.fragments_of_given_length.setdefault(key, [])
                    self.fragments_of_given_length[key].append(line)
        return self.fragments_of_given_length

    def draw_fragment_of_given_length(self, length, fr_type, db_path):
        """
        from self.fragments_of_given_length return random fragment
        """
        fragment_attributes = self.draw_fragment(length, fr_type, db_path)
        seq, sec_struct, length, anchoring_distance, frag_name, frag_struct = self.parse_fragment(fragment_attributes, db_path)
        frag = Fragment(seq, sec_struct, length, anchoring_distance, frag_name, frag_struct)
        return frag

    def draw_fragment(self, length, fr_type, db_path):
        """
        Picks a random protein fragment of specific length
        from the database.
        In: Dictionary of fragments
        Out: Random fragment with given length
        """

        if self.MOL_TYPE == "RNA":
            if fr_type == "n_term" or fr_type == "c_term":
                pdb_frag_name = self.fragments_of_given_length[length-1]
            #for internal fragments
            else:
                pdb_frag_name = self.fragments_of_given_length[length]
        if self.MOL_TYPE == "protein":
            if fr_type == "n_term" or fr_type == "c_term":
                pdb_frag_name = self.fragments_of_given_length[length+1]
            #for internal fragments
            else:
                pdb_frag_name = self.fragments_of_given_length[length+2]
  
        result = random.sample(pdb_frag_name,1)
        print "WYBRANY FRAGMENT", result
        #print result
        return result


    def parse_fragment(self, fragment_attributes, db_path):
        """
        """
        seq = self.retrieve_fragment_seq(fragment_attributes)
        sec_struct = self.retrieve_fragment_sec_struct(fragment_attributes)
        length = self.retrieve_fragment_length(fragment_attributes)
        anchoring_distance = self.retrieve_fragment_anchoring_distance(fragment_attributes)
        chain_id = self.retrieve_chain_id(fragment_attributes)
        frag_name = self.retrieve_fragment_name(fragment_attributes)
        frag_struct = self.retrieve_fragment_structure(db_path, frag_name, chain_id, fragment_attributes)
       

        return seq, sec_struct, length, anchoring_distance, frag_name, frag_struct

    def retrieve_fragment_seq(self, fragment_attributes):
        """
        """
        if self.MOL_TYPE == "RNA":
            return fragment_attributes[0][6]

        if self.MOL_TYPE == "protein":
            return fragment_attributes[0][10]
            
    def retrieve_fragment_sec_struct(self, fragment_attributes):
        """
        """
        if self.MOL_TYPE == "RNA":
            return fragment_attributes[0][7]
        if self.MOL_TYPE == "protein":
            return fragment_attributes[0][11]
    def retrieve_fragment_length(self, fragment_attributes):
        """
        """
        return int(fragment_attributes[0][0])

    def retrieve_fragment_anchoring_distance(self, fragment_attributes):
        """
        """
        if self.MOL_TYPE == "RNA":
            return float(fragment_attributes[0][8])
        if self.MOL_TYPE == "protein":
            return float(fragment_attributes[0][2])
        
    def retrieve_fragment_name(self, fragment_attributes):
        """
        """
        if self.MOL_TYPE == "RNA":
            return fragment_attributes[0][1]
        if self.MOL_TYPE == "protein":
            return fragment_attributes[0][1]+'_'+ fragment_attributes[0][7]+'-'+ fragment_attributes[0][9]+'_ok.pdb'
        
    def retrieve_fragment_structure(self, db_path, frag_name, chain_id, fragment_attributes):
        if self.MOL_TYPE == "RNA":
            start_end = range(int(fragment_attributes[0][3]), int(fragment_attributes[0][4])+1)
        if self.MOL_TYPE == "protein":
            start_end = range(int(fragment_attributes[0][7]), int(fragment_attributes[0][9])+1)#rozdzielenie na dwie bilbioteki, bo w inny miejscach nr reszt
        
        ex = Extres()
        ex.extract_residues(db_path+frag_name, 0, chain_id,start_end,True)
        return ex.chain_fragment
        
    def retrieve_chain_id(self, fragment_attributes):
        """
        """
        if self.MOL_TYPE == "RNA":
            return fragment_attributes[0][2]
        if self.MOL_TYPE == "protein":
            return fragment_attributes[0][5]


if __name__=='__main__':

    doc = """
    PyRy3D_Insert_Fragment


    (c) 2014 by Joanna M. Kasprzak, Mateusz Dobrychlop, Krzysztof Formanowicz, Jagoda Jablonska,
    Agnieszka Chelkowska, Weronika Kolczynska


    """
    
    #file_pdb = sys.argv[1]
    #file_fasta = sys.argv[2]
    #MOL_TYPE = sys.argv[3]
    
    
    
    
    print doc
    
    #set molecule type
    MOL_TYPE = "RNA" # "protein"
    #parse all input files
    file_fasta = 'tRNA_test_SS.fasta'
    file_pdb = 'tRNA_luki_1-8_25-30_38-42_72-_clean.pdb'
    db_path = 'clean_base/'
    db_file = 'LIR_fragments.lib'

    #MOL_TYPE = "protein"
    #file_fasta = 'chain_E.fasta'
    #file_pdb = 'E_nowe_zlukanakoncu.pdb'
    #file_frg = 'a_fragmencik.pdb'
    #db_path = 'baza\\'
    #db_file = 'proteinfr_new.lib'
    #outname = "out.pdb"

    results_loop = open("results.txt","w")
    
    for j in range(1):
        
        #try:
            outname = "aout/"+str(j)+".pdb"
            
            total_score = 0.0

            fp = FASTAParser(file_fasta)
            model_sequence = fp.get_sequence()
            model_sec_struct = fp.get_sec_struct()

            dp = DatabaseParser(db_file)
            fragments_of_given_length = dp.get_fragments_of_given_length()

            mp = ModelParser()
            component = mp.get_structure(file_pdb)
            gaps = mp.get_all_gaps(model_sequence, model_sec_struct)

            component_chain = component[0][mp.component_chain_id[0]]



            for i, gap in enumerate(gaps):
                temp_solution = 1 # revert after figuring out what the f is wrong with the db files
                while temp_solution == 1:
                    try:
                        frag = dp.draw_fragment_of_given_length(gap.length, gap.fr_type, db_path)
                        s = Superimpose_gap()
                        
                        if gap.fr_type == "internal":
                            s.superimpose_data_internal(component_chain, gap, frag)
                        elif gap.fr_type == "n_term":
                            s.superimpose_data_Nterm(component_chain, gap, frag)
                        else:
                            s.superimpose_data_Cterm(component_chain, gap, frag)
                        temp_solution = 0
                    except:
                        print "something went wrong, drawing another fragment"
                        temp_solution = 1

                s.calculate_rt_matrix()
                s.apply_rt_matrix(frag)
                s.rename_fragment(component, frag)
                
                s.renumber_fragment(frag, gap)

                if i == 0:
                    all_residues = s.merge_fragment_into_model(component, frag, gap.fr_type)
                else:
                    all_residues = s.merge_fragment_into_model(merged_structure, frag, gap.fr_type)

                merged_structure = s.build_merged_structure(all_residues, component)
                
                sc = Scorer()
                total_score += sc.score_sequence(gap, frag.sequence)
                total_score += sc.score_secondary_structure(gap, frag.sec_structure)
                sc.score_anchoring_dist(gap.distance, frag.dist_between_anchoring_residues)

            s.write_pdb(merged_structure, outname)
            
            
            print >> results_loop, outname, total_score, sc.rmsd_score, sc.seq_coverage, sc.sec_struct_coverage, sc.anchoring_distance
            
            
            print j, total_score
        
        #except:
            #print "ERRRORRR", j
        
     
    results_loop.close()
   
    
        #self.seq_coverage = 0.0
        #self.sec_struct_coverage = 0.0
        #self.rmsd_score = 0.0
        #self.anchoring_distance = 0.0
    